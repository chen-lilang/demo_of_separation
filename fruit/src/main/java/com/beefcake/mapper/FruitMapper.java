package com.beefcake.mapper;

import com.beefcake.entity.Fruit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2021-11-15
 */
public interface FruitMapper extends BaseMapper<Fruit> {

}
