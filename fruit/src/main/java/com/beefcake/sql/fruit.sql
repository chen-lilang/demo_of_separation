/*
 Navicat Premium Data Transfer

 Source Server         : link
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 15/11/2021 20:12:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fruit
-- ----------------------------
DROP TABLE IF EXISTS `fruit`;
CREATE TABLE `fruit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sale` int(11) NULL DEFAULT NULL,
  `icon` varchar(6000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fruit
-- ----------------------------
INSERT INTO `fruit` VALUES (1, '苹果', 222, 'https://cdn.pixabay.com/photo/2016/01/05/13/58/apple-1122537_960_720.jpg');
INSERT INTO `fruit` VALUES (2, '香蕉', 133, 'https://cdn.pixabay.com/photo/2021/10/07/15/24/fruits-6688947__340.jpg');
INSERT INTO `fruit` VALUES (3, '橘子', 434, 'https://cdn.pixabay.com/photo/2017/01/07/22/03/tangerines-1961725__340.jpg');
INSERT INTO `fruit` VALUES (6, '火龙果', 48, 'https://cdn.pixabay.com/photo/2017/07/06/10/04/fruit-2477516__340.jpg');
INSERT INTO `fruit` VALUES (19, '西瓜', 523, 'https://cdn.pixabay.com/photo/2015/06/19/16/48/watermelon-815072_960_720.jpg');
INSERT INTO `fruit` VALUES (21, '芒果', 140, 'https://cdn.pixabay.com/photo/2017/07/04/17/05/mango-2471837__340.jpg');
INSERT INTO `fruit` VALUES (22, '柠檬', 234, 'https://cdn.pixabay.com/photo/2017/07/07/12/34/lime-2481358__340.jpg');
INSERT INTO `fruit` VALUES (23, '草莓', 341, 'https://cdn.pixabay.com/photo/2018/04/29/11/54/strawberries-3359755__340.jpg');
INSERT INTO `fruit` VALUES (25, '葡萄', 240, 'https://cdn.pixabay.com/photo/2020/09/26/07/19/grapes-5603367__340.jpg');

SET FOREIGN_KEY_CHECKS = 1;
