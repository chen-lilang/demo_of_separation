package com.beefcake.controller;


import com.beefcake.entity.Fruit;
import com.beefcake.service.FruitService;
import com.beefcake.vo.BarVo;
import com.beefcake.vo.PieVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author admin
 * @since 2021-11-15
 */
@RestController
@RequestMapping("/fruit")
public class FruitController {
    @Autowired
    private FruitService fruitService;

    @GetMapping("/list")
    public List<Fruit> list() {
        return fruitService.list();
    }

    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Integer id) {
        return fruitService.removeById(id);
    }

    @GetMapping("/find/{id}")
    public Fruit find(@PathVariable("id") Integer id) {
        return fruitService.getById(id);
    }

    @PutMapping("/update")
    public boolean update(@RequestBody Fruit fruit) {
        return fruitService.updateById(fruit);
    }

    @PostMapping("/add")
    public boolean add(@RequestBody Fruit fruit) {
        return fruitService.save(fruit);
    }

    @GetMapping("/barVo")
    public BarVo barVo() {
        return fruitService.barVoList();
    }

    @GetMapping("/pieVo")
    public List<PieVo> pieVo() {
        return fruitService.PieVoList();
    }
}

