package com.beefcake.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beefcake.entity.Fruit;
import com.beefcake.vo.BarVo;
import com.beefcake.vo.PieVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2021-11-15
 */
public interface FruitService extends IService<Fruit> {
    BarVo barVoList();

    List<PieVo> PieVoList();
}
