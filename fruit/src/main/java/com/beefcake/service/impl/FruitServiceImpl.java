package com.beefcake.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beefcake.entity.Fruit;
import com.beefcake.mapper.FruitMapper;
import com.beefcake.service.FruitService;
import com.beefcake.util.DataUtil;
import com.beefcake.vo.BarVo;
import com.beefcake.vo.DataVo;
import com.beefcake.vo.PieVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author admin
 * @since 2021-11-15
 */
@Service
public class FruitServiceImpl extends ServiceImpl<FruitMapper, Fruit> implements FruitService {

    @Autowired(required = false)
    FruitMapper fruitMapper;

    @Override
    public BarVo barVoList() {
        BarVo barVo = new BarVo();
        List<String> names = new ArrayList<>();
        List<DataVo> values = new ArrayList<>();

        List<Fruit> fruits = fruitMapper.selectList(null);
        for (Fruit fruit : fruits) {
            names.add(fruit.getName());
            DataVo dataVo = new DataVo();
            dataVo.setValue(fruit.getSale());
            dataVo.setItemStyle(DataUtil.createItemStyle(fruit.getSale()));
            values.add(dataVo);
        }
        barVo.setNames(names);
        barVo.setValues(values);
        return barVo;
    }

    @Override
    public List<PieVo> PieVoList() {
        List<PieVo> pieVoList = new ArrayList<>();
        List<Fruit> fruits = fruitMapper.selectList(null);
        for (Fruit fruit : fruits) {
            PieVo pieVo = new PieVo();
            pieVo.setValue(fruit.getSale());
            pieVo.setName(fruit.getName());
            pieVo.setItemStyle(DataUtil.createItemStyle(fruit.getSale()));
            pieVoList.add(pieVo);
        }
        return pieVoList;
    }
}
