package com.beefcake.vo;

import lombok.Data;

import java.util.Map;

@Data
public class DataVo {
    private Integer value;
    private Map<String, String> itemStyle;

}
