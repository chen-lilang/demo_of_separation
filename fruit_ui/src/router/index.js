import Vue from 'vue'
import Router from 'vue-router'
import Bar from "../components/Bar";
import Pie from "../components/Pie";
import Table from "../components/Table";
import Edit from "../components/Edit";
import Add from "../components/Add";


Vue.use(Router)

export default new Router({
  routes: [
    {path: '/',redirect:'/table'},
    {path: '/bar',component:Bar},
    {path: '/pie',component:Pie},
    {path: '/table',component:Table},
    {path: '/edit',component:Edit},
    {path: '/add',component:Add},
  ]
})
